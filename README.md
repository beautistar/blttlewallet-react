# Bottle App

Bottle react native application. Note: must have macOS to build/run the iOS version, or Android Studio configured for Android.

# Installation

1. `npm install`
2. `npm run start`
   a. For Android: `npm run android`
   b. For iOS: `npm run ios`

## Tips

For iOS, make sure to install the pods:

```
$ cd ios
$ pod install
```

## Remote Metro Bundler

If you want to run macOS inside a virtual machine and connect the emulator within that VM to your host machine's metro bundler for live reloading, you just have to specify the remote URL for the metro bundler:

1. Shake the device (or do ctrl + D) to bring up the debug menu
2. Select "Change packer location"
3. Input the host/port of your computer running the metro packer (default port 8081)

## VSCode Setup

Add this to your user settings JSON:

```json
  "eslint.autoFixOnSave": true,
  "eslint.validate": [
    "javascript",
    "javascriptreact",
    {
      "language": "typescript",
      "autoFix": true
    },
    {
      "language": "typescriptreact",
      "autoFix": true
    }
  ],
```
