/*
 * Andrew Coutts
 * 2019
 *
 * This store handles i18n support by parsing JSON files which contain the strings
 * available for use throughout the UI. When a particular string isn't available
 * in a language, it falls back to english. If an english string isn't available
 * then it prepends  'ERR_' on the string to indicate in the UI there's an issue
 * for debugging purposes.
 *
 * Another neat feature of this module is that it supports templated strings in the language JSON.
 * If a JSON string contains a variable using handlebar format: {{var_a}}, it will replace the string
 * around it.
 *
 * Example:
 * JSON:
 * { "welcomeString": "Welcome to Bottlepay, {{username}}" }
 *
 * Output:
 * "Welcome to Bottlepay, bob"
 */
import {action, computed, observable} from "mobx"
import {CBaseStore} from "stores/BaseStore"
import {IRootStore} from "stores/RootStore"
import langEn from "root/localization/en.json"
import AsyncStorage from "@react-native-community/async-storage"

// Make languages available
const langsJson = {
  ...langEn,
} as any
type langType = keyof typeof langsJson

export class CLangStore extends CBaseStore {
  @observable currentSelectedLang = ""
  @observable langInitialized = false

  constructor(rootStore: IRootStore) {
    super(rootStore)

    // This returns a promise but we don't wait for it, we use the langInitialized variable
    // to track the status of this promise later on.
    this.asyncInitLang()
  }

  // ASyncStorage returns a promise and we can't do any async operations in the class
  // constructor so we have to do that here. We look in the app storage to see if there
  // is an existing lang set, and if so, we update the app to use that now.
  //
  // We use an observable "langInitialized" to keep track of whether or not we are done
  // initializing the language stuff. This will be used later on in AppStore to know if
  // we are done loading things during startup.
  asyncInitLang = async () => {
    // Set default language if it isn't already set
    const currentLang = await AsyncStorage.getItem("lang")
    if (!currentLang) {
      this.setLang("en") // Initialize default value
    } else {
      this.setLang(currentLang)
    }
    this.setLangInitialized(true)
  }

  @computed get safeGetCurrentLang() {
    // console.info("safeGetCurrentLang: ", Object.prototype.hasOwnProperty.call(langsJson, this.currentSelectedLang))
    return Object.prototype.hasOwnProperty.call(langsJson, this.currentSelectedLang) ? this.currentSelectedLang : "en"
  }

  // Safe method to set the current language on the TradingView chart and UI
  @action setLang = async (e: string) => {
    const newLang = Object.prototype.hasOwnProperty.call(langsJson, e) ? e : "en"
    // console.log("Setting lang: ", newLang)
    this.currentSelectedLang = newLang
    await AsyncStorage.setItem("lang", newLang)
  }

  @action setLangInitialized = (val: boolean) => (this.langInitialized = val)

  getAllLangs = (): Array<string> => {
    return Object.keys(langsJson)
  }

  getLangKeyNativeName = (key: string): string => {
    return langsJson[key as langType].langSelector.native
  }

  // This safely returns localized strings with fallback support to english in case the language is missing a particular string
  safeGetLocalizedString = (key: string): string => {
    // console.info("[safeGetLocalizedString] key: ", key)
    // Check if the current language has the requested key. If it doesn't have it, return the English fallback.
    // If the English fallback key doesn't exist either, return a generic error string indicating the issue.
    const localizedString = key.split(".").reduce((p, c) => (p && p[c]) || null, langsJson[this.safeGetCurrentLang as langType])
    if (!localizedString) {
      if (this.safeGetCurrentLang !== "en") {
        const fallBackString = key.split(".").reduce((p, c) => (p && p[c]) || null, langsJson.en)

        if (fallBackString) {
          console.error(`Warning: localized string missing for '${key}' - using fallback in 'en'`)
          return fallBackString
        }
      }
      console.error(`Warning: localized string missing for '${key}' - no fallback was available in 'en'`)
      return `<LANG_ERR_${key}>`
    }
    return localizedString
  }

  // Will return a safe localized string but also parse any variables passed in the variables object
  safeGetLocalizedStringParseVariables = (inputStringKey: string, variables: Map<string, string | number>) => {
    const stringVariables = variables // Don't mutate the function input

    // Retrieve the localized string containing the variable
    let returnString = this.safeGetLocalizedString(inputStringKey)

    // Parse all of the variables to replace them with their respective values from the map
    // First convert all number variables into strings
    for (const item of stringVariables.entries()) {
      // First convert the value into a string if it's a number so the regex replacement will work
      const varKey = item[0]
      const varValue = item[1].toString() // Convert to a string in case it's a number
      returnString = returnString.replace(new RegExp(`{{${varKey}}}`, "g"), varValue)
    }

    // Return the parsed string
    return returnString
  }
}
