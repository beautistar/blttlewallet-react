/*
 * Andrew Coutts
 * 2019
 *
 * This is the base store which all other stores implement which carries a reference to the
 * root store. This is what allows the stores to pass data / state between each other.
 */
import {IRootStore} from "./RootStore"

export class CBaseStore {
  // Create a generic reference to the root store
  protected readonly rootStore: IRootStore

  // When child class calls super() it will pass in the reference to rootStore
  protected constructor(rootStore: IRootStore) {
    this.rootStore = rootStore
  }
}
