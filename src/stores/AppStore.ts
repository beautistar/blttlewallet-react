/*
 * Andrew Coutts
 * 2019
 *
 * This is the AppStore, a very general store where usually I store things at first
 * and later on if I figure out a better way to organize it, I'll put it there otherwise.
 *
 * This is usually the proving ground for new features / funcionality.
 */

import {observable, action, computed} from "mobx"
import {CBaseStore} from "./BaseStore"
import {IRootStore} from "./RootStore"
import {Dimensions} from "react-native"

export class CAppStore extends CBaseStore {
  @observable isClicked = false // Globally-accessible variable now
  @observable fontLoaded = true // Set to true after fonts loaded
  @observable screenWidth = Dimensions.get("window").width
  @observable screenHeight = Dimensions.get("window").height
  @observable rem = (this.screenWidth / (0.54 * this.screenHeight)) * 16

  constructor(rootStore: IRootStore) {
    super(rootStore)
  }

  // Global "are things loading" computed value. Things from other stores feed into here
  // so we can keep track of the loading status of the app.
  @computed get isAppDoneLoading() {
    return this.rootStore.langStore.langInitialized && this.fontLoaded
  }

  @action setFontLoaded = (val: boolean) => (this.fontLoaded = val)
  @action toggleClicked = () => (this.isClicked = !this.isClicked)
}
