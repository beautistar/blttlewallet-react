/*
 * Andrew Coutts
 * 2019
 *
 * This is the root store. In the root store pattern, one root store is instantiated and holds
 * references to all of the other stores. When the root store instantiates the other stores, it
 * passes a reference to itself in the constructor of the stores.
 *
 * When the other stores call their constructor, they pass the root store reference in their super
 * constructor call to the base store, thereby saving a reference to the root store so it can be
 * used later on.
 *
 * Below is an example of how the AppStore can call a function in the LangStore:
 * ==> this.rootStore.langStore.someFunction()
 */
import {CAppStore} from "./AppStore"
import {CLangStore} from "./LangStore"

export interface IRootStore {
  appStore: CAppStore
  langStore: CLangStore
}

// Used to pass stores into components conveniently using a typescript interface
export interface IStoreProps {
  stores: IRootStore
}

// Configure strict mode in MobX. TODO: turn this on for production
// configure({ enforceActions: "always" })

export default class CRootStore implements IRootStore {
  appStore: CAppStore
  langStore: CLangStore

  constructor() {
    // Initialize the rest of the stores.
    // The ordering does matter and is carefully set in the order things are used within each store.
    this.appStore = new CAppStore(this)
    this.langStore = new CLangStore(this)
  }
}
