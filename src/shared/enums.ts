/*
 * Andrew Coutts
 * 2019
 *
 * Global enums used throughout the app
 */

export enum EScreens {
  welcomeScreen = "welcome",
  createWalletScreen = "createWallet",
}
