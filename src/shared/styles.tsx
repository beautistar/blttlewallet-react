/*
 * Andrew Coutts
 * 2019
 *
 * Shared styles all go in here
 */
import React from "react"
import styled from "styled-components"
import {Text, View} from "react-native"

// Primary purple color
export const brandColorPurple = "#22408A"

// Primary link / button pink color
export const brandColorPink = "#5E2A7A"

// Brand gradient
export const brandColorGradient = `linear-gradient(${brandColorPurple}, ${brandColorPink})`

// Primary payment / send button teal color
export const brandColorTeal = "#FF7030"

// Primary gray text color (light gray)
export const brandColorGrayText = "#657D94"

// Primary text color (navy blue)
export const brandColorPrimaryText = "#092D50"

// Text component which uses the Gilroy Medium font
export const TextMedium = styled(Text)`
  font-family: "gilroy-medium";
`

// Text component which uses the Gilroy Bold font
export const TextBold = styled(Text)`
  font-family: "gilroy-bold";
`

// Text component which uses the Gilroy Semi Bold font
export const TextSemiBold = styled(Text)`
  font-family: "gilroy-semibold";
`

// Standard container component for every view/screen
export const CommonViewContainer = styled(View)`
  background-color: #f2f2f2;
  justify-content: flex-end;
  align-items: center;
  flex: 1;
`

// Converts android elevation shadow styling to look similar to iOS box shadows
export const elevationShadowStyle = (elevation: number) => ({
  elevation,
})
