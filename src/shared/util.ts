// Utility functions
import * as util from "util"

// Inspect an object
export const inspect = (thing: any) => util.inspect(thing, {depth: null})
