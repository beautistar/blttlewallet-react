import React from "react"
import {View, TouchableHighlight, TextInput, ScrollView} from "react-native"
import styled from "styled-components"
import {inject, observer} from "mobx-react"
import {NavigationContainerProps} from "react-navigation"
import {IStoreProps} from "stores/RootStore"
import XIcon from "./x-icon.svg"
import {TextBold, brandColorPrimaryText, TextSemiBold, brandColorGrayText, brandColorPink} from "root/shared/styles"
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view"

// Extra step to render SVG with correct component name
const XIconSvg = (props: any) => <XIcon {...props} />

enum EWalletColor {
  pink = "94, 42, 122",
  teal = "0, 164, 153",
  orange = "237, 139, 0",
  yellow = "244, 192, 15",
  blue = "73, 142, 212",
}

enum ECircleOrder {
  first = "first",
  middle = "middle",
  last = "last",
}

export const ViewContainer = styled(KeyboardAwareScrollView)`
  background-color: #f2f2f2;
  flex: 1;
  padding-top: 25px;
`

const Title = styled(TextBold)`
  color: ${brandColorPrimaryText};
  font-size: 30px;
  letter-spacing: 0.18px;
  margin-bottom: 50px;
  padding: 0px 25px;
  margin-top: 20px;
`

const SubText = styled(TextSemiBold)`
  font-size: 17px;
  color: ${brandColorPrimaryText};
  letter-spacing: 0.29px;
  padding: 0px 25px;
`

const BackButton = styled(TouchableHighlight)`
  width: 60px;
  height: 60px;
  display: flex;
  justify-content: center;
  align-items: center;
  /* background: red; */
`

const WalletNameInput = styled(TextInput)`
  font-size: 20px;
  font-family: "gilroy-medium";
  letter-spacing: 0.41px;
  padding: 20px 0px;
  color: ${brandColorGrayText};
  width: 100%;
  padding: 0px 25px;
  margin: 20px 0px;
`

const DividerLine = styled(View)`
  justify-content: center;
  height: 1px;
  background-color: #979797;
  width: 100%;
  opacity: 0.1728;
`

const ColorSelectionTitle = styled(SubText)`
  margin-top: 41.5px;
`

const CirclesContainer = styled(ScrollView)`
  flex-direction: row;
`

const CircleTouchArea = styled(TouchableHighlight)`
  align-items: center;
  justify-content: center;
  height: 64px;
`

const OuterCircle = styled(View)`
  justify-content: center;
  align-items: center;
  width: 64px;
  height: 64px;
  border-radius: 50;
  background: ${(props: {color: EWalletColor; selected: boolean; order: ECircleOrder}) => (props.selected ? `rgba(${props.color}, 0.32)` : "transparent")};
  margin-right: ${(props: {color: EWalletColor; selected: boolean; order: ECircleOrder}) => (props.order === ECircleOrder.last ? "25px" : "40px")};
  margin-left: ${(props: {color: EWalletColor; selected: boolean; order: ECircleOrder}) => (props.order === ECircleOrder.first ? "25px" : "0px")};
`

const InnerCircle = styled(View)`
  width: 48px;
  height: 48px;
  border-radius: 50;
  background: ${(props: {color: EWalletColor; order: ECircleOrder}) => `rgba(${props.color}, 1)`};
`

const CreateButton = styled(View)`
  background: ${brandColorPink};
  border-radius: 50;
  padding: 20px 120px;
  margin-top: 80px;
  align-self: center;
  margin-bottom: 25px;
`

const CreateButtonText = styled(TextBold)`
  font-size: 17px;
  text-align: center;
  letter-spacing: 0.26px;
  color: #fff;
`

export default inject("stores")(
  observer(
    class CreateWallet extends React.Component<NavigationContainerProps & IStoreProps, {walletName: string; walletColor: EWalletColor}> {
      constructor(props: NavigationContainerProps & IStoreProps) {
        super(props)
        this.state = {
          walletName: "",
          walletColor: EWalletColor.pink,
        }
      }

      componentDidMount() {
        // alert(`${this.props.stores.appStore.screenWidth}, ${this.props.stores.appStore.screenHeight}`)
      }

      getCircle = (color: EWalletColor, order: ECircleOrder) => (
        <OuterCircle selected={this.state.walletColor === color} color={color} order={order}>
          <InnerCircle color={color} order={order} />
        </OuterCircle>
      )

      static navigationOptions = () => ({
        // headerLeft: (
        //   <HeaderContainer>
        //     <BackButton underlayColor="transparent" onPress={() => navigation.goBack()}>
        //       <XIcon fill={"red"} fillOpacity={1} />
        //     </BackButton>
        //   </HeaderContainer>
        // ),
        // headerStyle: {
        //   backgroundColor: "#F2F2F2",
        //   elevation: 0,
        //   shadowColor: "transparent",
        //   borderBottomWidth: 0,
        //   overflow: "hidden",
        // },
        header: null,
      })

      setSelectedWalletColor = (color: EWalletColor) => this.setState({walletColor: color})

      render() {
        return (
          <ViewContainer>
            <BackButton underlayColor="transparent" onPress={() => this.props.navigation!.goBack()}>
              <XIconSvg fill={"red"} fillOpacity={1} />
            </BackButton>

            <Title>Create Wallet</Title>

            <SubText>Enter wallet name</SubText>

            <WalletNameInput
              placeholder={"My Personal Wallet"}
              // autoFocus={true}
            />

            <View style={{paddingLeft: 25, paddingRight: 25, width: "100%"}}>
              <DividerLine />
            </View>

            <ColorSelectionTitle>Select a color for your wallet</ColorSelectionTitle>

            <View style={{height: 64, marginTop: 30}}>
              <CirclesContainer horizontal={true} showsHorizontalScrollIndicator={false}>
                <CircleTouchArea underlayColor="transparent" onPress={() => this.setSelectedWalletColor(EWalletColor.pink)}>
                  {this.getCircle(EWalletColor.pink, ECircleOrder.first)}
                </CircleTouchArea>

                <CircleTouchArea underlayColor="transparent" onPress={() => this.setSelectedWalletColor(EWalletColor.teal)}>
                  {this.getCircle(EWalletColor.teal, ECircleOrder.middle)}
                </CircleTouchArea>

                <CircleTouchArea underlayColor="transparent" onPress={() => this.setSelectedWalletColor(EWalletColor.orange)}>
                  {this.getCircle(EWalletColor.orange, ECircleOrder.middle)}
                </CircleTouchArea>

                <CircleTouchArea underlayColor="transparent" onPress={() => this.setSelectedWalletColor(EWalletColor.yellow)}>
                  {this.getCircle(EWalletColor.yellow, ECircleOrder.middle)}
                </CircleTouchArea>

                <CircleTouchArea underlayColor="transparent" onPress={() => this.setSelectedWalletColor(EWalletColor.blue)}>
                  {this.getCircle(EWalletColor.blue, ECircleOrder.last)}
                </CircleTouchArea>
              </CirclesContainer>
            </View>

            <CreateButton>
              <CreateButtonText>Create Wallet</CreateButtonText>
            </CreateButton>
          </ViewContainer>
        )
      }
    },
  ),
)
