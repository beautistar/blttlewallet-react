import "react-native"
import React from "react"
import CreateWallet, {ViewContainer} from "screens/CreateWallet"
import RootStore from "stores/RootStore"
import {shallow} from "enzyme"
import {View, Text} from "react-native"

const rootStore = new RootStore()

it("renders correctly", () => {
  const tree = shallow(<CreateWallet.wrappedComponent stores={rootStore} />)
  expect(tree).toMatchSnapshot()
  expect(tree.find(ViewContainer).length).toBe(1)
})
