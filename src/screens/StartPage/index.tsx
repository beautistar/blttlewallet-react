import React from "react"
import {View, Image, Text, StyleSheet, Button, TouchableHighlight} from "react-native"
import styled from "styled-components"
import {NavigationContainerProps} from "react-navigation"
import {inject, observer} from "mobx-react"
import {IStoreProps} from "stores/RootStore"

import {TextMedium, TextBold, brandColorGrayText, brandColorPink, brandColorPrimaryText, CommonViewContainer, elevationShadowStyle, brandColorGradient, brandColorPurple} from "root/shared/styles"

import {EScreens} from "root/shared/enums"
import LinearGradient from "react-native-linear-gradient"
import Logo from "./Wallet.svg"

const MyBottleWalletbox = styled(View)`
  height: 150;
  border-radius: 155;
  width: 155;
  border-width: 0;
  background-color: white;
  overflow: hidden;
  margin-bottom: 40px;
`

const AddButton = styled(View)`
  background: ${brandColorPink};
  border-radius: 50;
  padding: 20px 120px;
  margin-top: 80px;
  align-self: center;
  margin-bottom: 25px;
`

const AddButtonText = styled(TextBold)`
  font-size: 17px;
  text-align: center;
  letter-spacing: 0.26px;
  color: #fff;
`
const NewWalletCreated = styled(TextBold)`
font-size: 20
text-align: center;
letter-spacing: 0.26px;
 color: #000;
margin-bottom:20;
font-weight:bold;
`
const NewWalletCreatedText = styled(Text)`
  font-size: 13px;
  text-align: center;
  letter-spacing: 0.26px;
  color: #6a7a8a;
`

const MaybeLater = styled(View)`
  font-size: 13px;
  text-align: center;
  letter-spacing: 0.26px;
  color: black;
  margin-bottom: 5;
`

export default inject("stores")(
  observer(
    class StartPage extends React.Component<NavigationContainerProps & IStoreProps> {
      static navigationOptions = {
        header: null,
      }

      render() {
        return (
          <CommonViewContainer style={{paddingBottom: 5 * this.props.stores.appStore.rem}}>
            <MyBottleWalletbox>
              <Logo width={250} height={250} style={{marginLeft: -50}} />
            </MyBottleWalletbox>

            <NewWalletCreated>New Wallet Created!</NewWalletCreated>

            <NewWalletCreatedText>
              <Text>
                Start Adding Contact For Your {"\n"}
                Wallet to get started
              </Text>
            </NewWalletCreatedText>

            <AddButton>
              <AddButtonText>
                <Text>Add Contacts</Text>
              </AddButtonText>
            </AddButton>

            <MaybeLater>
              <Text>Maybe later</Text>
            </MaybeLater>
          </CommonViewContainer>
        )
      }
    },
  ),
)

// We use the built-in style sheet to create shadows on Android
const styles = StyleSheet.create({
  boxShadow: {
    ...elevationShadowStyle(1),
  },
})
