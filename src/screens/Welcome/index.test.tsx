import "react-native"
import React from "react"
import WelcomeScreen from "screens/Welcome"
import RootStore from "stores/RootStore"
import {shallow} from "enzyme"
import {CommonViewContainer} from "root/shared/styles"

const rootStore = new RootStore()

it("renders correctly", () => {
  const tree = shallow(<WelcomeScreen.wrappedComponent stores={rootStore} />)
  expect(tree).toMatchSnapshot()
  expect(tree.find(CommonViewContainer).length).toBe(1)
})
