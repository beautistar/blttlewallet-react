import React from "react"
import {View, Image, StyleSheet, TouchableHighlight} from "react-native"
import styled from "styled-components"
import {NavigationContainerProps} from "react-navigation"
import {inject, observer} from "mobx-react"
import {IStoreProps} from "stores/RootStore"
import {TextMedium, TextBold, brandColorGrayText, brandColorPink, brandColorPrimaryText, CommonViewContainer, elevationShadowStyle, brandColorGradient, brandColorPurple} from "root/shared/styles"
import FileAdd from "./common-file-add.svg"
import BIcon from "./b-light.svg"
import {EScreens} from "root/shared/enums"
import LinearGradient from "react-native-linear-gradient"

// SVGs
const FileAddSvg = (props: any) => <FileAdd {...props} />
const BottleSvg = (props: any) => <BIcon {...props} />

const Title = styled(TextBold)`
  font-size: ${(props: {rem: number}) => 1.75 * props.rem};
  color: ${brandColorPrimaryText};
  font-weight: bold;
  margin-top: ${(props: {rem: number}) => 2 * props.rem};
`

const SubTitle = styled(TextMedium)`
  font-size: ${(props: {rem: number}) => 1 * props.rem};
  color: #6a7a8a;
  padding: ${(props: {rem: number}) => `0px ${3.125 * props.rem}px`};
  text-align: center;
  margin-top: ${(props: {rem: number}) => `${0.4375 * props.rem}px`};
`

const Button = styled(View)`
  flex-direction: row;
  box-shadow: ${(props: {rem: number}) => `0px ${0.125 * props.rem}px ${0.75 * props.rem}px rgba(213, 213, 213, 0.359075)`};
  padding: ${(props: {rem: number}) => `${1.5625 * props.rem}px`};
  background: #fff;
  border-radius: ${(props: {rem: number}) => `${0.625 * props.rem}px`};
  margin-bottom: ${(props: {rem: number}) => `${1 * props.rem}px`};
`

const ButtonLast = styled(Button)`
  margin-bottom: 0;
`

const ButtonContainer = styled(View)`
  margin-top: ${(props: {rem: number}) => `${3 * props.rem}px`};
`

const ButtonTitle = styled(TextBold)`
  color: ${brandColorPink};
  font-size: ${(props: {rem: number}) => `${1.0625 * props.rem}px`};
`

const ButtonSubTitle = styled(TextMedium)`
  font-size: ${(props: {rem: number}) => `${0.875 * props.rem}px`};
  color: ${brandColorGrayText};
`

const ButtonImage = styled(Image)`
  margin-right: ${(props: {rem: number}) => `${1.25 * props.rem}px`};
`

const BottleBackground = styled(View)`
  display: flex;
  justify-content: center;
  align-items: center;
`

export default inject("stores")(
  observer(
    class WelcomeScreen extends React.Component<NavigationContainerProps & IStoreProps> {
      static navigationOptions = {
        header: null,
      }

      render() {
        return (
          <CommonViewContainer style={{paddingBottom: 2 * this.props.stores.appStore.rem}}>
            {/* <Image source={require("./bottle-logo.png")} /> */}
            {/* <BottleIconSvg width={9.375 * this.props.stores.appStore.rem} height={9.375 * this.props.stores.appStore.rem} /> */}
            <BottleBackground>
              <LinearGradient
                colors={["#004C97", "#6D2077"]}
                start={{x: 0, y: 0}}
                end={{x: 1, y: 1}}
                locations={[0, 0.7]}
                style={{
                  borderRadius: 35,
                  width: 10.5 * this.props.stores.appStore.rem,
                  height: 10.5 * this.props.stores.appStore.rem,
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}>
                <BottleSvg width={5.5 * this.props.stores.appStore.rem} height={5.5 * this.props.stores.appStore.rem} />
              </LinearGradient>
            </BottleBackground>
            {/*  */}

            <Title rem={this.props.stores.appStore.rem}>{this.props.stores.langStore.safeGetLocalizedString("screens.welcome.welcome")}</Title>
            <SubTitle rem={this.props.stores.appStore.rem}>{this.props.stores.langStore.safeGetLocalizedString("screens.welcome.subtitle")}</SubTitle>

            <ButtonContainer rem={this.props.stores.appStore.rem}>
              <TouchableHighlight onPress={() => this.props.navigation!.navigate(EScreens.createWalletScreen)} underlayColor={"transparent"}>
                <Button style={styles.boxShadow} rem={this.props.stores.appStore.rem}>
                  <FileAddSvg fill={brandColorPink} fill-opacity={0} width={40} style={{marginRight: 20}} />
                  <View>
                    <ButtonTitle rem={this.props.stores.appStore.rem}>{this.props.stores.langStore.safeGetLocalizedString("screens.welcome.createWallet")}</ButtonTitle>
                    <ButtonSubTitle rem={this.props.stores.appStore.rem}>{this.props.stores.langStore.safeGetLocalizedString("screens.welcome.createSubtitle")}</ButtonSubTitle>
                  </View>
                </Button>
              </TouchableHighlight>

              <ButtonLast style={styles.boxShadow} rem={this.props.stores.appStore.rem}>
                <ButtonImage rem={this.props.stores.appStore.rem} source={require("./import-wallet.png")} />
                <View>
                  <ButtonTitle rem={this.props.stores.appStore.rem}>{this.props.stores.langStore.safeGetLocalizedString("screens.welcome.importWallet")}</ButtonTitle>
                  <ButtonSubTitle rem={this.props.stores.appStore.rem}>{this.props.stores.langStore.safeGetLocalizedString("screens.welcome.importSubtitle")}</ButtonSubTitle>
                </View>
              </ButtonLast>
            </ButtonContainer>
          </CommonViewContainer>
        )
      }
    },
  ),
)

// We use the built-in style sheet to create shadows on Android
const styles = StyleSheet.create({
  boxShadow: {
    ...elevationShadowStyle(1),
  },
})
