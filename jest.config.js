module.exports = {
  preset: "react-native",
  moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json", "node"],
  testEnvironmentOptions: {
    enzymeAdapter: "react16",
  },
  setupFilesAfterEnv: ["jest-enzyme", "<rootDir>/jest/setup-async-storage.js", "<rootDir>/jest/setup-enzyme.js"],
}
