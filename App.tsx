import React from "react"
import {createAppContainer} from "react-navigation"
import {createStackNavigator} from "react-navigation-stack"
import Welcome from "screens/Welcome"
import CreateWallet from "screens/CreateWallet"
import {Provider, observer} from "mobx-react"
import RootStore from "stores/RootStore"
import {Text, View} from "react-native"
import styled from "styled-components"
import StartPage from "screens/StartPage"

const LoadingContainer = styled(View)`
  justify-content: center;
  align-items: center;
  flex: 1;
`

// Initialize a new instance of the root store
const rootStore = new RootStore()

const AppNavigator = createStackNavigator(
  {
    welcome: Welcome,
    createWallet: CreateWallet,
    startpage: StartPage,
  },
  {
    initialRouteName: "startpage",
  },
)

const AppContainer = createAppContainer(AppNavigator)

export default observer(
  class App extends React.Component {
    render() {
      if (rootStore.appStore.isAppDoneLoading) {
        return (
          <Provider stores={rootStore}>
            <AppContainer />
          </Provider>
        )
      } else {
        return (
          <LoadingContainer>
            <Text>Loading..</Text>
          </LoadingContainer>
        )
      }
    }
  },
)
